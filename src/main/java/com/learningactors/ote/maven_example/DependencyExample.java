package com.learningactors.ote.maven_example;

public class DependencyExample {
    public static String helloDependency() {
        return "Hello From Dependency!";
    }

    public static int addInt(int a, int b) {
        return a + b;
    }

    public static int multiplyInt(int a, int b) {
        return a * b;
    }
}

