package com.learningactors.ote.maven_example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DependencyExampleTest  {

    DependencyExample dependencyExample = new DependencyExample();

    @Test
    public void should_return_hello() {
        String result = DependencyExample.helloDependency();

        assertEquals("Hello From Dependency!", result);
    }

    @Test
    public void should_calculate_add() {
        int result = DependencyExample.addInt(3, 7);

        assertEquals(10, result);
    }

    @Test
    public void should_calculate_multiplication() {
        int result = DependencyExample.multiplyInt(3, 7);

        assertEquals(21, result);
    }

}